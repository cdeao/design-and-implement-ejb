//Cameron Deao
//CST-235
//1/21/2019

package controllers;

import javax.faces.bean.*;
import beans.Order;
import beans.User;
import business.OrdersBusinessInterface;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import java.util.Map;
import javax.inject.Inject;
import javax.ejb.EJB;

@ManagedBean
@ViewScoped

public class FormController {
	
	@Inject
	OrdersBusinessInterface x;
	@EJB
	business.MyTimerService timer;
	Order order = new Order("TEST", "NEW TEST", 1, 2);
	public String onSubmit(User user) {
		timer.setTimer(10000);
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, Object> params = fc.getExternalContext().getRequestMap();
		user = (User) params.get("user");
		//Calls the method within the Order.java to fill the list.
		order.newOder(order);
		x.test();
		x.setOrders(null);
		
		return "TestResponse.xhtml";
	}
	
	public String onFlash(User user) {
		FacesContext fc = FacesContext.getCurrentInstance();
		Flash params = fc.getExternalContext().getFlash();
		user = (User) params.put("user", this);
		return "TestResponse2.xhtml";
	}
	
	public OrdersBusinessInterface getService() {
		return x;
	}
	
	public String goBack() {
		return "TestForm.xhtml";
	}
}

