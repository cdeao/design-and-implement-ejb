//Cameron Deao
//CST-235
//1/21/2019
package business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.Order;
import beans.Orders;
/**
 * Session Bean implementation class AnotherOrdersBusinessService
 */
@Alternative
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
public class AnotherOrdersBusinessService implements OrdersBusinessInterface {
	

	List<Order> orders = new ArrayList<>();
	
    /**
     * Default constructor. 
     */
    public AnotherOrdersBusinessService() {
    	orders.add(new Order("TESTING", "TESTNAME", 55, 35));
    }

	/**
     * @see OrdersBusinessInterface#test()
     */
    public void test() {
    	System.out.println("Hello from AnotherOrdersBusinessService");
    }

	public List<Order> getOrder() {
		return orders;
	}

	
	public void setOrders(List<Orders> orders) {
		
	}
}
