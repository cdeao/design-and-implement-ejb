//Cameron Deao
//CST-235
//1/21/2019
package business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.Order;
import beans.Orders;
/**
 * Session Bean implementation class OrdersBusinessService
 */
@Alternative
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
public class OrdersBusinessService implements OrdersBusinessInterface {

	
	List<Order> orders = new ArrayList<>();
    /**
     * Default constructor. 
     */
    public OrdersBusinessService() {
    	orders.add(new Order("SECOND TEST NUMBER", "SECOND TEST NAME", 22, 33));
    }

	/**
     * @see OrdersBusinessInterface#test()
     */
    public void test() {
       System.out.println("Hello from the OrdersBusinessService");
    }


	public List<Order> getOrder() {
		return orders;
	}

	
	public void setOrders(List<Orders> orders) {
	
	}
}
