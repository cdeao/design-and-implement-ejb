//Cameron Deao
//CST-235
//1/21/2019
package business;

import java.util.List;
import javax.ejb.*;
import beans.*;

@Local
public interface OrdersBusinessInterface {
	
	public List<Order> getOrder();
	public void setOrders(List<Orders> orders);
	public void test();
}
