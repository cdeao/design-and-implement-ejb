//Cameron Deao
//CST-235
//1/27/2019
package beans;


import java.util.ArrayList;
import javax.faces.bean.*;

@ManagedBean(name = "Orders", eager = true)
@ViewScoped
public class Orders{
	
	//Establishing the list.
	//Getters and setters are used for the list.
	public static ArrayList<Order> orders = new ArrayList<Order>();

	public void setOrdersList(ArrayList<Order> orders) {
		Orders.orders = orders;
	}
	
	public ArrayList<Order> getOrder() {
		return orders;
	}
}
