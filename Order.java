//Cameron Deao
//CST-235
//1/27/2019
package beans;

public class Order {
	
	//Variables are initialized with dummy data to display.
	public String orderNumber;
	public String productName;
	public float productPrice;
	public int productQuantity;

	

	public Order(String orderNumber, String productName, int productPrice, int productQuantity) {
		this.orderNumber = orderNumber;
		this.productName = productName;
		this.productPrice = productPrice;
		this.productQuantity = productQuantity;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public void setProductPrice(float productPrice) {
		this.productPrice = productPrice;
	}
	
	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}
	
	public String getOrderNumber() {
		return orderNumber;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public float getProductPrice() {
		return productPrice;
	}
	
	public int getProductQuantity() {
		return productQuantity;
	}
	
	//Method that is used for adding an order into the list.
	public Order newOder(Order order) {
		Orders.orders.add(order);
		return order;
	}
}