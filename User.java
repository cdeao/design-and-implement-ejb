//Cameron Deao
//CST-235
//1/21/2019

package beans;
import javax.faces.bean.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ManagedBean
@ViewScoped
public class User{

	@NotNull() @Size(min=5, max=15)
	public String firstName;
	@NotNull() @Size(min=5, max=15)
	public String lastName;
	
	
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
}